---
Title: "Contact"
type: "page"
menu:
  sidebar:
    weight: 100
---

<div class="jumbotron">
  <h1 class="display-4">Get in touch.</h1>
  <p class="lead">
    Take a moment to tell us about your ideas. We're excited to help you
    succeed.
  </p>
  <hr class="my-4">
  <a class="btn btn-primary btn-lg" href="mailto:mike@cooperativeengineering.org?subject=I'm ready to succeed with custom software." role="button">
      Email Mike
  </a>
</div>
