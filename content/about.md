---
Title: "About"
type: "page"
menu:
  sidebar:
    weight: -100
draft: false
---

# Who We Are

We are **Cooperative Engineering**, a self-directed group of leaders,
engineers, and designers with the vision, knowledge, and experience to craft
and ship your high-impact software.

**We're different from other consulting shops.** The doers of Cooperative
Engineering have a direct interest in making your software succeed. Why?
Because those doers are the owners. Every employee-owner of our cooperative
succeeds when *you* succeed. Beyond skill and experience, the professionals
crafting your software have the agency and ownership stake that other
consultants will never have.
