---
title: "Styleguide"
date: 2018-11-07T16:40:23-06:00
draft: true
resources:
- name: article-image
  src: article-image.png
authors:
- Michael Wilson
---

# Shortcodes
## alert

A [bootstrap alert box](http://getbootstrap.com/docs/4.1/components/alerts/#examples).
A way to create nice context-reflective info boxes.

```plaintext
{{%/* alert [type="danger"] [headline="My Headline] [icon="fas fa-cannabis"*/%}}
My markdown...
{{%/* /alert */%}}
```

### Examples

{{% alert %}}
<i class="fas fa-quote-left"></i>
*Linux-PAM is a system of libraries that handle the authentication tasks of
applications [...] on the system. The library provides a stable general
interface [...] that privilege granting programs [...] defer to to perform
standard authentication tasks.*
{{% /alert %}}

{{% alert type="danger" headline="Important" %}}
If the system you're configuring for 2FA is one to which you're connected by
SSH, be sure to leave an SSH session open while you're making changes until
you're confident that your changes are correct, otherwise you risk being locked
out.
{{% /alert %}}

{{% alert type="warning" headline="Other Colors" %}}
Check out the [Bootstrap docs](http://getbootstrap.com/docs/4.1/components/alerts/#examples) for more colors.

Also, paragraph spacing is handled so that there's not a big ugly vertical
blank space after the last paragraph.
{{% /alert %}}

{{% alert type="success" headline="Icons R Kewl" icon="fas fa-cannabis" %}}
Hey, you can use icons if you want to.
{{% /alert %}}
