---
title: "Enhancing SSH Auth with 2FA"
date: 2018-11-05T14:46:29-06:00
draft: false
resources:
- name: article-image
  src: puf200X172.gif
authors:
- Michael Wilson
---

SSH is perhaps the most essential system management tool for UNIX users, but
its utility necessarily comes with a huge risk: any user able to gain SSH
access to a system can exert enormous control over it, potentially even
getting root.

Use of public key cryptography with SSH is offered as a means to effectively
eliminate the possibility of guessed or brute-force attacks on passwords.
However, if a system forces users to authenticate with public keys instead of
passwords, a measure of convenience is lost; users must be in possession of
their private key at all times they wish to access the target system. This
poses a problem for users who cannot or do not wish to be permanently attached
to the computer on which their SSH key resides.

Fortunately, with some additional tooling SSH can be augmented to enforce
password-based authentication while additionally requiring a valid [time-based
one time password
(TOTP)](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm).
This scheme is known as 2FA (two factor auth) or MFA (multi factor auth), and
commonly associated with [Google
Authenticator](https://en.wikipedia.org/wiki/Google_Authenticator).


# Prerequisites

First off, this article assumes you're running some sort of Linux on the system
serving SSH, but exact instructions will be provided for Arch Linux (current as
of date of publication) and Ubuntu (18.04 LTS). I am bound to make
generalizations about the configuration in Linux (e.g. locations of files)
which do not uniformly apply to all distributions. Please note that I
intentionally omit privilege escalation (i.e., `sudo`) here and there; it's for
your own good. Copy/pasting `sudo` commands given to you by internet strangers
is bad.

We'll need to install two packages:

* [google-authenticator-libpam](https://github.com/google/google-authenticator-libpam)
* [qrencode](https://fukuchi.org/works/qrencode/)

Arch:
```bash
pacman -Sy libpam-google-authenticator qrencode
```

Ubuntu:
```bash
apt install libpam-google-authenticator qrencode
```


# Meet PAM

In Linux, PAM is an authentication abstraction; an acronym for *Pluggable
Authentication Modules*. From [`man 8 pam`](https://linux.die.net/man/8/pam):

{{% alert %}}
<i class="fas fa-quote-left"></i>
*Linux-PAM is a system of libraries that handle the authentication tasks of
applications [...] on the system. The library provides a stable general
interface [...] that privilege granting programs [...] defer to to perform
standard authentication tasks.*
{{% /alert %}}

PAM is the software which implements the authentication system used by
SSH, local console logins, and the like.

The configurations for PAM reside in the directory `/etc/pam.d`, e.g.:

```bash
/etc/pam.d/
├── atd
├── chfn
├── chpasswd
├── chsh
├── common-account
├── common-auth
├── common-password
├── common-session
├── common-session-noninteractive
├── cron
├── login
├── newusers
├── other
├── passwd
├── polkit-1
├── runuser
├── runuser-l
├── sshd
├── su
├── sudo
├── systemd-user
└── vmtoolsd
```

Hey, `sshd` is in there!

# Configuration

## PAM

Edit `/etc/pam.d/sshd` and add `auth required pam_google_authenticator.so`
before the first uncommented line. In Ubuntu this will look something like:

{{< highlight pamconf "hl_lines=3-4" >}}
# PAM configuration for the Secure Shell service

# add the line below
auth required pam_google_authenticator.so

# Standard Un*x authentication.
@include common-auth

# ...
{{< /highlight >}}

In Arch, the full `/etc/pam.d/sshd` will be:

{{< highlight pamconf "hl_lines=3-4" >}}
#%PAM-1.0
#auth     required  pam_securetty.so     #disable remote root
# add the line below
auth      required  pam_google_authenticator.so
auth      include   system-remote-login
account   include   system-remote-login
password  include   system-remote-login
session   include   system-remote-login
{{< /highlight >}}

## SSH

For both Ubuntu and Arch, edit `/etc/ssh/sshd_config` and swap

```sshdconfig
ChallengeResponseAuthentication no
```

for

```sshdconfig
ChallengeResponseAuthentication yes
```

Alternatively `sed` can be used to perform the edit in-place:

```bash
sed -i 's/\(ChallengeResponseAuthentication\) no/\1 yes/' /etc/ssh/sshd_config
```

What does the `ChallengeResponseAuthentication` flag enable? [`man 5
sshd_config`](https://linux.die.net/man/5/sshd_config) is rather cryptic:

{{% alert %}}
<i class="fas fa-quote-left"></i>
*Specifies whether challenge-response authentication is allowed (e.g. via PAM
or through authentication styles supported in login.conf(5)) The default is
yes.*
{{% /alert %}}

For now let it suffice that `ChallengeResponseAuthentication` enables `sshd` to
directly collect text from the client interactively for authentication purposes
(hmm, maybe for something like 2FA?). See Further Reading for
additional information regarding `ChallengeResponseAuthentication`.

Then we're going to instruct `sshd` to honor either public key auth **or**
password + 2FA by appending the following onto `/etc/ssh/sshd_config`:

```sshdconfig
AuthenticationMethods publickey keyboard-interactive:pam
```

Before moving on reload the `sshd` configuration:

```bash
$ systemctl reload sshd
```


## google-authenticator

The last configuration task is to generate a shared secret which will be used
by `pam_google_authenticator.so` when generating TOTPs. This process must be
repeated for **all** user accounts which are to be accessible by SSH. For this
purpose the `libpam-google-authenticator` package ships with the executable
`google-authenticator`. Let's give it a go:

```plaintext

$ google-authenticator

Do you want authentication tokens to be time-based (y/n) y
Warning: pasting the following URL into your browser exposes the OTP secret to Google:
  https://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/vagrant@ubuntu-bionic%3Fsecret%3DSRIYKAPAVRGQ5LCQDZA76E26J4%26issuer%3Dubuntu-bionic


[QR CODE HERE]


Your new secret key is: SRIYKAPAVRGQ5LCQDZA76E26J4
Your verification code is 725191
Your emergency scratch codes are:
  84361003
  59359098
  10824956
  32205166
  95413892

Do you want me to update your "/home/vagrant/.google_authenticator" file? (y/n) y

Do you want to disallow multiple uses of the same authentication
token? This restricts you to one login about every 30s, but it increases
your chances to notice or even prevent man-in-the-middle attacks (y/n) y

By default, a new token is generated every 30 seconds by the mobile app.
In order to compensate for possible time-skew between the client and the server,
we allow an extra token before and after the current time. This allows for a
time skew of up to 30 seconds between authentication server and client. If you
experience problems with poor time synchronization, you can increase the window
from its default size of 3 permitted codes (one previous code, the current
code, the next code) to 17 permitted codes (the 8 previous codes, the current
code, and the 8 next codes). This will permit for a time skew of up to 4 minutes
between client and server.
Do you want to do so? (y/n) y

If the computer that you are logging into isn't hardened against brute-force
login attempts, you can enable rate-limiting for the authentication module.
By default, this limits attackers to no more than 3 login attempts every 30s.
Do you want to enable rate-limiting? (y/n) y
```

This process is fairly self-explanatory, and results in a few things worth noting:

* a UTF-8 printed QR code (not pictured) which can be scanned by any 2FA app
* the generated secret key
* the first generated verification code
* a number of emergency scratch codes which can be used in place of TOTPs
  generated by your 2FA device in case it is lost or destroyed
* a new file, `~/.google_authenticator`, containing your secret hey as well as
  other settings


# Test Run

{{% alert type="danger" headline="Important" icon="fas fa-exclamation-triangle" %}}
If the system you're configuring for 2FA is one to which you're connected by
SSH, be sure to leave an SSH session open while you're making changes until
you're confident that your changes are correct, otherwise you risk being locked
out.
{{% /alert %}}

Let's try logging in:

```plaintext
$ ssh -i /dev/null some-cool-host
Load key "/dev/null": invalid format
Verification code:
Password:
Last login: Mon Nov  5 23:09:44 2018 from fe80::fa63:3fff:fea0:bac7%enp3s0
```

We explicitly ask ssh to connect with an invalid key (i.e., `/dev/null`) in
order to test out 2FA + password auth.

We can try again using a passwordless SSH key (Bad! Don't use passwordless SSH
keys!):

```plaintext
$ ssh -i ~/.ssh/my-key some-cool-host
Load key "/dev/null": invalid format
Verification code:
Password:
Last login: Mon Nov  5 23:12:29 2018 from fe80::fa63:3fff:fea0:bac7%enp3s0
```


# Closing Remarks

Adding TOTP 2FA as a supplement to password authentication in SSH can add
meaningful security, but I find something about the setup to be slightly
misleading: do we really now require another factor in our auth scheme?
Supposedly we have something we know (our password) and something we have (our
2FA device). But in fact, our 2FA device itself is not an essential component
in this scheme, only just a container for another secret: the secret generated
by `google-authenticator`. It seems that in the end we still only have one
factor, but with two secrets.

Being that TOTP 2FA works on the basis of a shared secret, it's important to
consider the trust you're putting in the authenticator software you'll use for
generating TOTP codes. Or equivalently, consider that malicious authenticator
software could phone home with your shared secret. Incompetent software could
fail to adequately protect your shared secret at rest, potentially exposing it
to other software. My advice would be to use well-regarded open source
authenticator software, such as [andOTP](https://github.com/andOTP/andOTP).

Even if it's not truly 2FA, I still think adding `google-authenticator` to your
SSH auth scheme adds meaningful security. Gaining an understanding of PAM also
enables the use of many other sorts of authentication. If you seek truly
distinct factors of auth,
[Yubikey](https://developers.yubico.com/yubico-pam/YubiKey_and_SSH_via_PAM.html)
is an option worth investigating.


# Further Reading

* [google-authenticator PAM Module Instructions](https://github.com/google/google-authenticator/wiki/PAM-Module-Instructions)
* [Arch Wiki - Google Authenticator](https://wiki.archlinux.org/index.php/Google_Authenticator#Desktop_logins)
* [SSH PasswordAuthentication vs ChallengeResponseAuthentication](https://blog.tankywoo.com/linux/2013/09/14/ssh-passwordauthentication-vs-challengeresponseauthentication.html)
