---
title: "Generating Secure Passwords"
date: 2018-11-28T01:26:59Z
resources:
- name: article-image
  src: keys.jpg
authors:
- Michael Wilson
---

Obviously strong passwords are necessary, but what are some easy ways to
generate them on the UNIX-like system you're on? Read on.

<!--more-->

## 256-bit hexadecimal

All self-respecting UNIX-like OSes will include `head`, `md5sum` and `awk` in
their userland so the following solution is useful for maximum compatibility:

```bash
$ head --bytes=32 /dev/urandom | md5sum | awk '{ print $1 }'
15b3567d1a349c205656b10bed1e929b
```

## 256-bit base64

`base64` packs more randomness per character than hexadecimal:

```bash
$ head --bytes=32 /dev/urandom | base64
fMkuJvxp5JQ5+xFnFfJqSBn08J6XCtC09308BL3w2Fg=
```

If you want to eliminate special characters (potentially at the expense of a
few bits of randomness):

```bash
$ head --bytes=32 /dev/urandom | base64 | sed 's/\W//g'
kBTQErYM3TW8T4Wra88lrCHjDie3PSdUxv5S9vzI
```

## Memorable passwords

```bash
$ shuf /usr/share/dict/cracklib-small | head -4 | tr '\n' '-' | sed 's/-$/\n/'
spokes-eccentricities-neglected-prowled
```

I also like this method for generating completely random usernames when signing
up for stuff I don't want easily associated with my identity. For me it's kind
of hard to pull a few random words out of the ether which don't say anything
about me.

{{% alert type="warning" headline="Important" icon="fas fa-exclamation-triangle" %}}
On my machine, `cracklib-small` clocks in at 54,763 words, giving
8,992,932,673,964,378,160 possible four-word permutations. While this may seem
adequate, it's less than 63 bits of randomness.

For the paranoid, either add more words or [expand your word
list](https://github.com/cracklib/cracklib/tree/master/words/files).
{{% /alert %}}

## Use a dedicated program

Check out [`pwgen`](https://sourceforge.net/projects/pwgen/) if you're looking
for customizable options.

```bash
$ pwgen 16
quoo6aebe0feiXoh voo3Ohfoovahsh6o chohPheeChiQu3sh hahjuxa1Moog9ieH
iecohh6Oophoh1IS kaToi3ahquoogai5 vaehohCeita1iroh ahpooGae2ahm9us4
thoph9iek1Ib9Shi uL3ofooqueeyeith vougo2Lakai0ceet giPuTui3oolaePha
noow7eigae2eeB3b oeb4Oowiepoghol3 aaWoo8eecaphem0k ael1ooW2engoor6c
eeb2ohw6phie7ohX ahToo0thei7ietoo phiem1ahdiewu7Ir aet2ruoKiew3quou
eesoh7Wieyeang2z ahHu2aich6Eevio8 ejeeh4Zaeph9Tah8 taqua8zob9xoh5Ai
aiRaigh9baidosoh TheeP6ahxaij0iek aeTooquoonae4gae Vae3ievei0Ohqu8f
eingu5Yae6eejoox seec1yahjah2lieL io1dee5shoo9Fahu daesh7zai6wiph4W
Daivooy8iexa5ahf Il6yae4Tee3jeiP4 BeiTanguaxa6nohH eesh0Vae3vai6Ahr
Phiele0po0taeF0e xaeloofaitahP4ie joh6XahcohshaiJo chohT8ohb5Aino4n
aihieFiekuo9ee9a Pheph6saoPhe1moh thie5eiS3xeey9sh Loophee8toh3mi4P
oochi0tuaYoo7OGh eba9Ahhei8eiRius EeP2yei1maquiozu Jahgh5xoh7oceif0
ohziije8ohsh9UuM noo1aeyieM2chai0 Phah2Ohh0pahrae8 Eikailiex6godiPu
Jaishaij7Air0Iew il9sohghaiNo2mee Leiquahvu7xah9ki Mi5yaifie5ingee8
sooKei4aika4cie9 Reesha1aiJeechae neik4Ijoh9Vowu5a ohK7Aiphi0wuu3wo
xus5eeDee4onoonu goo5JeLae9bieQuo Zei7aaqu4Yah9ohp ceeGhohzeish0eif
och5fith1cee6Mah Saat6achi1iemeik zae4gaeYoos6ki8h ohb8oiPahX1ahghe
AengooYei4duvu1a gai3UzeVieni5hee AePiJaeh4ohnguac Toovoov7aej2OhMo
ahxaeth9Ea0veib2 Hue0eim1ohphahlu heep7aiPh8shataf aK6puf1ieshohwai
yahrohcie2RiC0yu OoKo2tar8Ooleedi Paix9iewohw5ohFo xeachaGee2hereeh
```

## Other Recommendations

**Use a password manager**. I recommend [KeePass](https://keepass.info/). It
can generate lengthy, randomized passwords, so you can forget about this silly
article. Unlike password manager services like LastPass, KeePass stores your
passwords in an on-disk database. Plus it's open source so if you're really
inclined you can audit the code. Combine KeePass with
[Syncthing](https://syncthing.net/) to synchronize your password database
across all your devices. Get [Kee](https://www.kee.pm/) for convenient integration
with Firefox. Finally, get
[keepass2android](https://github.com/PhilippC/keepass2android) and now you have
all of your passwords with you all the time.

But of course there are times when a using a password manager is impractical or
impossible. Like, what will you use to manage the password for your password
manager? In these circumsatances consider the
[XKCD advice](https://www.xkcd.com/936/).

Final note: we, the internet community, should just get rid of passwords and
password managers and make
[WebAuthn](https://developer.mozilla.org/en-US/docs/Web/API/Web_Authentication_API)
a thing.
