---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true
resources:
- name: article-image
  src: article-image.png
authors:
- Author Name
---
